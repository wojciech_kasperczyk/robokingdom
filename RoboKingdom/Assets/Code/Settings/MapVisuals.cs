﻿using System;
using System.Collections.Generic;
using Map;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Rendering;
using UnityEngine;
using UnityEngine.Rendering;
using Random = UnityEngine.Random;

namespace Settings
{
    [CreateAssetMenu(menuName = "Robo Kingdom/Map Visuals")]
    public class MapVisuals : ScriptableObject
    {
        [SerializeField]
        private float _tilesVisualOffset;

        [SerializeField]
        private GameObject _grassTilePrototype;
        
        [SerializeField]
        private Color[] _grassTileColorVariances;

        public float TilesVisualOffset => _tilesVisualOffset;

        public MapVisualsData CreateMapVisualsData()
        {
            MapVisualsData data = new MapVisualsData();

            var grassTileColorVariances = _grassTileColorVariances;
            
            data.AddTileInstanceRenderer(MapTileType.Grass,
                _grassTilePrototype.GetComponent<MeshInstanceRendererComponent>().Value,
                grassTileColorVariances);
            
            return data;
        }
    }

    public class MapVisualsData
    {
        private readonly Dictionary<MapTileType,List<MeshInstanceRenderer>> _tileVariances = new Dictionary<MapTileType, List<MeshInstanceRenderer>>();

        public MeshInstanceRenderer GetTileInstanceRenderer(MapTileType tileType,int varianceSeed = 0)
        {
            var variances = GetVariances(tileType);
            if (variances.Count == 0)
            {
                return new MeshInstanceRenderer();
            }

            if (variances.Count == 1)
            {
                return variances[0];
            }
            
            int varianceIndex = Random.Range(0,variances.Count);

            return variances[varianceIndex];
        }
        
        public void AddTileInstanceRenderer(MapTileType tileType, MeshInstanceRenderer instanceRenderer)
        {
            AddTileInstanceRenderer(tileType,instanceRenderer,new Color[0]);
        }

        public void AddTileInstanceRenderer(MapTileType tileType, MeshInstanceRenderer instanceRenderer, Color[] colorVariances)
        {
            List<MeshInstanceRenderer> variances = GetVariances(tileType);
            variances.Add(instanceRenderer);
            for (int i = 0; i < colorVariances.Length; i++)
            {
                Material m = new Material(instanceRenderer.material);
                m.color = colorVariances[i];
                var variance = new MeshInstanceRenderer() 
                    {
                        mesh = instanceRenderer.mesh,
                        material = m,
                        castShadows = instanceRenderer.castShadows,
                        receiveShadows = instanceRenderer.receiveShadows,
                        subMesh = instanceRenderer.subMesh
                        
                    };
                variances.Add(variance);
            }
        }

        private List<MeshInstanceRenderer> GetVariances(MapTileType tileType)
        {
            List<MeshInstanceRenderer> variances;
            if (!_tileVariances.TryGetValue(tileType, out variances))
            {
                variances = new List<MeshInstanceRenderer>();
                _tileVariances.Add(tileType,variances);
            }

            return variances;
        }
    }
}