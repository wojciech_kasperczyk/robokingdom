﻿using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

namespace Settings
{
    [CreateAssetMenu(fileName = "GameSettings", menuName = "Robo Kingdom/Game Settings")]
    public class GameSettings : ScriptableObject
    {
        [SerializeField]
        private int2 _mapSize;

        [SerializeField] private int _mapSeed;
        
        [SerializeField]
        private MapVisuals _mapVisuals;
        
        [SerializeField]
        private CharacterVisuals _characterVisuals;

        [SerializeField] 
        private int _startingCharacters;
        
        public int2 MapSize => _mapSize;
        public int StartingCharacters => _startingCharacters;
        public MapVisuals MapVisuals => _mapVisuals;
        public CharacterVisuals CharacterVisuals => _characterVisuals;
        public int MapSeed => _mapSeed;
    }
}