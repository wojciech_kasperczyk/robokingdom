﻿using Unity.Rendering;
using UnityEngine;

namespace Settings
{
    [CreateAssetMenu(menuName = "Robo Kingdom/Character Visuals")]
    public class CharacterVisuals : ScriptableObject
    {
        [SerializeField] 
        private GameObject _characterPrototype;

        public CharacterVisualsData CreateCharacterVisualsData()
        {
            CharacterVisualsData visualsData = new CharacterVisualsData();

            visualsData.CharacterRendering = _characterPrototype.GetComponent<MeshInstanceRendererComponent>().Value;
            
            return visualsData;
        }
    }

    public class CharacterVisualsData
    {
        public MeshInstanceRenderer CharacterRendering;
    }
}