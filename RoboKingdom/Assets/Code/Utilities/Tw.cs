﻿namespace Utilities
{
    public static class Tw
    {
        public static float Linear(float t) { return t; }

        public static float Flip(float t) { return 1 - t; }
        
        public static float SmoothStart2(float t) { return t*t; }

        public static float SmoothStart3(float t) { return t*t*t; }

        public static float SmoothStart4(float t) { return t*t*t*t; }

        public static float SmoothStop2(float t)
        {
            return Flip( SmoothStart2( Flip(t)));
        }
        
        public static float SmoothStop3(float t)
        {
            return Flip( SmoothStart3( Flip(t)));
        }
        
        public static float SmoothStop4(float t)
        {
            return Flip( SmoothStart4( Flip(t)));
        }

        public static float SmoothStep2(float t)
        {
            return Mix(SmoothStart2(t), SmoothStop2(t), t);
        }

        public static float SmoothStep3(float t)
        {
            return Mix(SmoothStart3(t), SmoothStop3(t), t);
        }

        public static float SmoothStep4(float t)
        {
            return Mix(SmoothStart4(t), SmoothStop4(t), t);
        }

        public static float Arch2(float t)
        {
            return Scale(Flip(t), t);
        }

        public static float SmoothStartArch3(float t)
        {
            return Scale(Arch2(t),t);
        }
        
        public static float SmoothStopArch3(float t)
        {
            return ReverseScale(Arch2(t),t);
        }
        
        public static float SmoothStepArch4(float t)
        {
            return ReverseScale(Scale(Arch2(t),t),t);
        }
        
        private static float Mix(float a, float b, float weightB)
        {
            return a * (1 - weightB) + b * weightB;
        }

        private static float Scale(float a, float t)
        {
            return a * t;
        }
        
        private static float ReverseScale(float a, float t)
        {
            return a * (1-t);
        }
        
    }
}