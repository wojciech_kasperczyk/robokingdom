﻿using System.Runtime.CompilerServices;

namespace Utilities
{
    public static class Noise
    {
        private const int N1 = 2059714729;
        private const int N2 = 1759714717;
        private const int N3 =  859714753;

        private const float IntSpan = (float)int.MaxValue - (float)int.MinValue;
        
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static int N(int n, int seed = 0)
        {
            unchecked
            {
                n *= N1;
                n += seed;
                n ^= n >> 8;
                n += N2;
                n ^= n << 8;
                n *= N3;
                n ^= n >> 8;
            }

            return n;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static float GetFloatZeroToOne(int n, int seed = 0)
        {
            float val = N(n,seed);
            
            return val /IntSpan;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static float GetFloatMinusOneToOne(int n, int seed = 0)
        {
            return ((N(n,seed)/ IntSpan) * 2) - 1;
        }
    }
}