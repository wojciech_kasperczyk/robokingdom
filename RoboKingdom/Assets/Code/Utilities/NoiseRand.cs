﻿namespace Utilities
{
    public class NoiseRand
    {
        private int _state = int.MinValue;
        private int _seed;

        public void SetSeed(int seed)
        {
            _seed = seed;
        }
        
        public int Next()
        {
            unchecked
            {
                return Noise.N(++_state,_seed);
            }
        }

        public float RandomZeroToOne()
        {
            unchecked
            {
                return Noise.GetFloatZeroToOne(++_state, _seed);
            }
        }
        
        public float RandomMinusOneToOne()
        {
            unchecked
            {
                return Noise.GetFloatMinusOneToOne(++_state, _seed);
            }
        }
    }
}