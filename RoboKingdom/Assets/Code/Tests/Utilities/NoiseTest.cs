﻿using UnityEngine;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;
using Utilities;

public class NoiseTest 
{
    [Test]
    [TestCase(0)]
    [TestCase(-1)]
    [TestCase(int.MaxValue)]
    [TestCase(int.MinValue)]
    [TestCase(1)]
    public void Noise_GetFloatZeroToOne_Passes(int n)
    {
        float value = Noise.GetFloatZeroToOne(n);
        Assert.LessOrEqual(value, 1.0f);
        Assert.GreaterOrEqual(value, 0f);
    }

    [Test]
    [TestCase(0)]
    [TestCase(-1)]
    [TestCase(int.MaxValue)]
    [TestCase(int.MinValue)]
    [TestCase(1)]
    public void Noise_N_CalledTwiceReturnsSameValue(int n)
    {
        int valueA = Noise.N(n);
        int valueB = Noise.N(n);
        Assert.AreEqual(valueA,valueB);
    }
    
    [Test]
    [TestCase(0,0,1)]
    [TestCase(-1,0,1)]
    [TestCase(int.MaxValue,0,1)]
    [TestCase(int.MinValue,0,1)]
    [TestCase(1,0,1)]
    public void Noise_N_CalledTwiceWithDifferentSeedReturnsDifferentValue(int n, int seedA, int seedB)
    {
        int valueA = Noise.N(n,seedA);
        int valueB = Noise.N(n,seedB);
        Assert.AreNotEqual(valueA,valueB);
    }
}
