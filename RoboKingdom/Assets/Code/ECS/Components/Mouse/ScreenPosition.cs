﻿using Unity.Entities;
using Unity.Mathematics;

namespace Components.Mouse
{
    public struct ScreenPosition : IComponentData
    {
        public float2 Value;
    }
}