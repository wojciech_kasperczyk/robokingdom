﻿using Unity.Entities;
using Unity.Mathematics;

namespace Components.Mouse
{
    public struct DeltaScreenPosition : IComponentData
    {
        public float2 Value;
        public float LastTimeUpdated;
    }
}