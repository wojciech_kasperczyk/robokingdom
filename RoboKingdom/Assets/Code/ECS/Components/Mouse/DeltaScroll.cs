﻿using Unity.Entities;

namespace Components.Mouse
{
    public struct DeltaScroll : IComponentData
    {
        public float Value;
        public float LastTimeUpdated;

    }
}