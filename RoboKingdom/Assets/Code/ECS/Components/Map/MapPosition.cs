﻿using Unity.Entities;
using Unity.Mathematics;

namespace Components.Map
{
    public struct MapPosition : IComponentData
    {
        public int2 Value;
    }
}