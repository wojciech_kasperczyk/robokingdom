﻿using Unity.Entities;

namespace Components.Map
{
    public struct MapSize : IComponentData
    {
        public int Width;
        public int Height;
    }
}