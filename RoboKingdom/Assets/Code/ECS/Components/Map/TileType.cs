﻿using Map;
using Unity.Entities;

namespace Components.Map
{
    public struct TileType : IComponentData
    {
        public MapTileType Value;
    }
}