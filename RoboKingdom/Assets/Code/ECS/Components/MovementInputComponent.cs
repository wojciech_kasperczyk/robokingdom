﻿using Unity.Entities;

public struct MovementInput : IComponentData
{
    public float Horizontal;
    public float Vertical;
}

public class MovementInputComponent : ComponentDataWrapper<MovementInput>
{
}