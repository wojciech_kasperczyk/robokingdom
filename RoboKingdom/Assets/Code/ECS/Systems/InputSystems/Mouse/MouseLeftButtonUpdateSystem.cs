﻿using Systems.Groups;
using Components.Mouse;
using Unity.Entities;
using UnityEngine;

namespace Systems.InputSystems.Mouse
{
    [UpdateInGroup(typeof(InputCollectionGroup))]
    public class MouseLeftButtonUpdateSystem : ComponentSystem
    {
        private const int LEFT_MOUSE_BUTTON = 0;
        
        private struct NoButtonData
        {
            public readonly int Length;
            public EntityArray Entities;
            public ComponentDataArray<MouseState> MouseStates;
            public SubtractiveComponent<LeftMouseButton> ExcludeLeftMouseButtons;
        }

        private struct ButtonData
        {
            public readonly int Length;
            public EntityArray Entities;
            public ComponentDataArray<MouseState> MouseStates;
            public ComponentDataArray<LeftMouseButton> LeftMouseButtons;
            
        }
        
        [Inject] private NoButtonData _noButtonData;
        [Inject] private ButtonData _buttonData;
        
        protected override void OnUpdate()
        {
            if (Input.GetMouseButton(LEFT_MOUSE_BUTTON) && !Input.GetMouseButtonDown(LEFT_MOUSE_BUTTON))
            {
                for (int i = 0; i < _noButtonData.Length; i++)
                {
                    EntityManager.AddComponentData(_noButtonData.Entities[i],new LeftMouseButton());
                }    
            }
            else
            {
                for (int i = 0; i < _buttonData.Length; i++)
                {
                    EntityManager.RemoveComponent<LeftMouseButton>(_buttonData.Entities[i]);
                } 
            }
            
        }
    }
}