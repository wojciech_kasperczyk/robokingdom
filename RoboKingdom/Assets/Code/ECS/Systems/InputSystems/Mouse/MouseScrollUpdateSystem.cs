﻿using Systems.Groups;
using Components.Mouse;
using Unity.Entities;
using UnityEngine;

namespace Systems.InputSystems.Mouse
{
    [UpdateInGroup(typeof(InputCollectionGroup))]
    public class MouseScrollUpdateSystem : ComponentSystem
    {
        private struct Data
        {
            public readonly int Length;
            public ComponentDataArray<MouseState> MouseStates;
            public ComponentDataArray<DeltaScroll> DeltaScrolls;
        }

        [Inject] private Data _data;

        protected override void OnUpdate()
        {
            var deltaScroll = Input.mouseScrollDelta.y;
            
            var now = Time.time;
            
            for (int i = 0; i < _data.Length; i++)
            {
                _data.DeltaScrolls[i] = new DeltaScroll(){ Value = deltaScroll, LastTimeUpdated = now};
            }
        }
    }
}