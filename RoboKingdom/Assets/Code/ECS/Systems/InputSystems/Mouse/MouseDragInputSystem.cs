﻿using Systems.Groups;
using Components.Mouse;
using Unity.Collections;
using Unity.Entities;

namespace Systems.InputSystems.Mouse
{
    [UpdateInGroup(typeof(InputGroup))]
    public class MouseDragInputSystem : ComponentSystem
    {
        private struct MovementData
        {
            public readonly int Length;
            public ComponentDataArray<MovementInput> MovementInputs;
        }

        private struct MouseDragData
        {
            public readonly int Length;
            [ReadOnly] public ComponentDataArray<MouseState> MouseStateFlags;
            [ReadOnly] public ComponentDataArray<DeltaScreenPosition> DeltaScreenPositions;
            [ReadOnly] public ComponentDataArray<LeftMouseButton> LeftMouseButtonFlags;
        }
        
        [Inject] private MovementData _movementData;
        [Inject] private MouseDragData _mouseDragData;
        
        protected override void OnUpdate()
        {
            for (int i = 0; i < _mouseDragData.Length; i++)
            {
                var deltaPosition = _mouseDragData.DeltaScreenPositions[i].Value;
                var horizontal = deltaPosition.x*-100;
                var vertical = deltaPosition.y*-100;

                for (var j = 0; j < _movementData.Length; j++)
                {
                    _movementData.MovementInputs[j] = new MovementInput() { Horizontal = horizontal, Vertical = vertical};
                }
            }
        }
    }
}