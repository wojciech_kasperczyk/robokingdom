﻿using System.Runtime.InteropServices;
using Systems.Groups;
using Components;
using Components.Mouse;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

namespace Systems.InputSystems.Mouse
{
    [UpdateInGroup(typeof(InputCollectionGroup))]
    public class MousePositionUpdateSystem : ComponentSystem
    {
        private struct Data
        {
            public readonly int Length;
            public ComponentDataArray<MouseState> MouseStates;
            public ComponentDataArray<ScreenPosition> ScreenPositions;
            public ComponentDataArray<DeltaScreenPosition> DeltaScreenPositions;
        }

        [Inject] private Data _data;

        protected override void OnUpdate()
        {
            
            var mousePosition = Input.mousePosition;
            var newPosition = new float2(mousePosition.x/Screen.width, mousePosition.y/Screen.height);
            var now = Time.time;
            
            for (int i = 0; i < _data.Length; i++)
            {
                var oldPosition = _data.ScreenPositions[i].Value;
                var deltaPosition = newPosition - oldPosition; 

                _data.ScreenPositions[i] = new ScreenPosition() { Value = newPosition};
                _data.DeltaScreenPositions[i] = new DeltaScreenPosition(){Value = deltaPosition, LastTimeUpdated = now};
            }
        }
    }
}