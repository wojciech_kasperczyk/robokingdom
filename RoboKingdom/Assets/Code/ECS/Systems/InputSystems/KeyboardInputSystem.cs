﻿using Systems.Groups;
using Unity.Entities;
using UnityEngine;

[UpdateInGroup(typeof(InputGroup))]
public class KeyboardInputSystem : ComponentSystem
{
    [Inject] private Data _data;

    private struct Data
    {
        public readonly int Length;
        public ComponentDataArray<MovementInput> MovementInputs;
    }

    protected override void OnUpdate()
    {
        var horizontal = Input.GetAxis("Horizontal");
        var vertical = Input.GetAxis("Vertical");

        for (var i = 0; i < _data.Length; i++)
        {
            var movementInput = _data.MovementInputs[i];
            movementInput.Horizontal = horizontal;
            movementInput.Vertical = vertical;
            _data.MovementInputs[i] = movementInput;
        }
    }
}