﻿using Systems.Groups;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

[UpdateInGroup(typeof(GameplayGroup))]
public class CameraMovementSystem : ComponentSystem
{
    [Inject] private Data _data;

    protected override void OnUpdate()
    {
        for (var i = 0; i < _data.Length; i++)
        {
            var movementInput = _data.MovementInputs[i];

            var dir = new float3(movementInput.Horizontal, 0, movementInput.Vertical);

            var position = _data.Positions[i];
            var rotation = _data.Rotations[i];

            position.Value += math.mul(rotation.Value, dir * 10 * Time.deltaTime);

            _data.Positions[i] = position;
        }
    }

    private struct Data
    {
        public readonly int Length;
        [ReadOnly] public ComponentDataArray<MovementInput> MovementInputs;
        [ReadOnly] public ComponentDataArray<Rotation> Rotations;
        public ComponentDataArray<Position> Positions;
    }
}