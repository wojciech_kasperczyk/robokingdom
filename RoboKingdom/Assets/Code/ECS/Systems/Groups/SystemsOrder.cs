﻿using System;
using Unity.Entities;

namespace Systems.Groups
{
    public class InputCollectionGroup {}
    
    [UpdateAfter(typeof(InputCollectionGroup))]
    public class InputGroup {}
    
    [UpdateAfter(typeof(InputGroup))]
    public class GameplayGroup {}
    
}