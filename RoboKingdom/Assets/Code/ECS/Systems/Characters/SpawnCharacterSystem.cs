﻿using Components.Characters;
using Components.Map;
using Map;
using Settings;
using Unity.Collections;
using Unity.Entities;
using Unity.Rendering;
using Unity.Transforms;

namespace Code.ECS.Systems.Characters
{
    public class SpawnCharacterSystem : ComponentSystem
    {
        private struct CreationRequestData
        {
            public readonly int Length;
            [ReadOnly] public ComponentDataArray<SpawnCharacterRequest> RequestFlags;
            [ReadOnly] public ComponentDataArray<MapPosition> MapPositions;
            [ReadOnly] public EntityArray Entities;
        }

        private struct MapInfoData
        {
            public readonly int Length;
            [ReadOnly] public ComponentDataArray<MapInfo> MapInfoFlags;
            [ReadOnly] public ComponentDataArray<MapSize> MapSizes;
        }
        
        [Inject] private CreationRequestData _requests;
        [Inject] private MapInfoData _mapInfos;
        
        private CharacterVisualsData _characterVisualsData;
        private EntityArchetype _characterArchetype;
        private MapVisuals _mapVisuals;

        public void SetVisuals(CharacterVisualsData characterVisualsData,MapVisuals mapVisuals)
        {
            _characterVisualsData = characterVisualsData;
            _mapVisuals = mapVisuals;
        }

        protected override void OnStartRunning()
        {
            base.OnStartRunning();
            _characterArchetype = EntityManager.CreateArchetype(
                ComponentType.Create<Character>(),
                ComponentType.Create<Position>(),
                ComponentType.Create<Rotation>(),
                ComponentType.Create<TransformMatrix>(),
                ComponentType.Create<MeshInstanceRenderer>(),
                ComponentType.Create<MapPosition>());
        }

        protected override void OnUpdate()
        {
            var requestsLength = _requests.Length;
            
            if (_mapInfos.Length == 0 || requestsLength == 0)
                return;

            var mapSize = _mapInfos.MapSizes[0];
            for (int i = 0; i < requestsLength; i++)
            {
                var mapPosition = _requests.MapPositions[i];
                PostUpdateCommands.CreateEntity(_characterArchetype);
                PostUpdateCommands.SetComponent( mapPosition);
                PostUpdateCommands.SetComponent( new Position(MapHelper.MapToWorldPosition(mapPosition.Value,mapSize.Width,mapSize.Height,_mapVisuals.TilesVisualOffset)));
                PostUpdateCommands.SetSharedComponent(_characterVisualsData.CharacterRendering);
                PostUpdateCommands.DestroyEntity(_requests.Entities[i]);
            }
            
            

        }
    }
}