﻿using System.Collections.Generic;
using System.Diagnostics;
using Code.ECS.Systems.Characters;
using Components.Characters;
using Components.Map;
using Components.Mouse;
using Map;
using Settings;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;
using Utilities;
using Debug = UnityEngine.Debug;

namespace Code.ECS
{
    public class GameplayManager : MonoBehaviour
    {
        private EntityManager _entityManager;
        
        public EntityArchetype MouseStateArchetype { get; private set; }
        
        public EntityArchetype CharacterSpawnRequestArchetype { get; private set; }
        
        [SerializeField]
        private GameSettings _settings;

        private CharacterVisualsData _characterVisualsData;

        private void Start()
        {
            _entityManager = World.Active.GetOrCreateManager<EntityManager>();
            
            CreateArchetypes();
            InitializeGame();
            
            
        }

        private void CreateArchetypes()
        {
            CreateMouseStateArchetype();
            CreateCharacterSpawnRequestArchetype();
        }

        private void CreateCharacterSpawnRequestArchetype()
        {
            CharacterSpawnRequestArchetype = _entityManager.CreateArchetype(
                ComponentType.Create<SpawnCharacterRequest>(),
                ComponentType.Create<MapPosition>()
            );
        }

        private void CreateMouseStateArchetype()
        {
            MouseStateArchetype = _entityManager.CreateArchetype(
                ComponentType.Create<MouseState>(),
                ComponentType.Create<ScreenPosition>(),
                ComponentType.Create<DeltaScreenPosition>(),
                ComponentType.Create<DeltaScroll>()
            );
        }

        private void InitializeGame()
        {
            InitializeMouse();
            InitializeMap();
            InitializeCharacters();
        }

        private void InitializeMouse()
        {
            var mouseEntity = _entityManager.CreateEntity(MouseStateArchetype);
            var mousePosition = Input.mousePosition;
            _entityManager.SetComponentData(mouseEntity, new ScreenPosition {Value = new float2(mousePosition.x, mousePosition.y)});
            _entityManager.SetComponentData(mouseEntity, new DeltaScreenPosition() {Value = new float2(0,0), LastTimeUpdated = Time.time});
            _entityManager.SetComponentData(mouseEntity, new DeltaScroll() {Value = 0, LastTimeUpdated = Time.time});
            
        }

        private void InitializeMap()
        {
            var map = MapGenerator.Generate(_settings.MapSize.x, _settings.MapSize.y, MapTileType.Grass);
            map.GenerateEntities(_entityManager, _settings.MapVisuals);
        }

        private void InitializeCharacters()
        {
            _characterVisualsData = _settings.CharacterVisuals.CreateCharacterVisualsData();
            
            World.Active.GetOrCreateManager<SpawnCharacterSystem>().SetVisuals(_characterVisualsData,_settings.MapVisuals);
            
            SpawnCharacters(_settings.StartingCharacters);
        }

        private void SpawnCharacters(int charactersCount)
        {
            HashSet<int2> spawinngPositions = new HashSet<int2>();

            int2 positionGenerationOrigin = new int2(_settings.MapSize.x / 2,_settings.MapSize.y / 2);
            positionGenerationOrigin -= new int2(charactersCount/2);
            
            for (int i = 0; i < charactersCount; i++)
            {
                int2 newPos;
                do
                {
                    newPos = new int2(Random.Range(0,charactersCount),Random.Range(0,charactersCount));
                } 
                while (spawinngPositions.Contains(newPos));

                spawinngPositions.Add(newPos);
                SpawnCharacter(newPos + positionGenerationOrigin);
            }
        }
        
        private void SpawnCharacter(int2 mapPosition)
        {
            var characterEntity = _entityManager.CreateEntity(CharacterSpawnRequestArchetype);
            _entityManager.SetComponentData(characterEntity, new MapPosition(){ Value = mapPosition });
        }
    }
}