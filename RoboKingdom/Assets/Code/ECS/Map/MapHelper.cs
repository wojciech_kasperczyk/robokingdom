﻿using Unity.Mathematics;

namespace Map
{
    public static class MapHelper
    {
        public static float3 MapToWorldPosition(int2 position, int mapWidth, int mapHeight, float tileSize)
        {
            float2 center = new float2(mapWidth * 0.5f, mapHeight * 0.5f);

            float2 offsetFromCenter = position - center;

            return new float3(offsetFromCenter.x * tileSize, 0, offsetFromCenter.y * tileSize);
        }
    }
}