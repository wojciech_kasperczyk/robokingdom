﻿using UnityEngine;

namespace Map
{
    public static class MapGenerator
    {
        public static MapPrototype Generate(int width, int height, MapTileType mapTileType)
        {
            return Generate(width, height, new FillRule(mapTileType));
        }

        public static MapPrototype Generate(int width, int height, GeneratorRule rule)
        {
            if (width <= 0 || height <= 0)
            {
                throw new UnityException(
                    $"Map with size ({width} x {height}) can't be generated. Width and Hegight of a map must be grater than 0!");
            }
            var map = new MapPrototype(width,height);
            rule.Generate(map);
            return map;
        }
    }
}