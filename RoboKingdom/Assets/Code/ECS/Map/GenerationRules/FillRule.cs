﻿namespace Map
{
    public class FillRule : GeneratorRule
    {
        private MapTileType _mapTileType;

        public FillRule(MapTileType fillType)
        {
            _mapTileType = fillType;
        }

        public override void Generate(MapPrototype mapPrototype)
        {
            for (int x = 0; x < mapPrototype.Width; x++)
            {
                for (int y = 0; y < mapPrototype.Heigth; y++)
                {
                    mapPrototype.TilesPrototype[x, y].TileType = _mapTileType;
                }
            }
        }
    }
}