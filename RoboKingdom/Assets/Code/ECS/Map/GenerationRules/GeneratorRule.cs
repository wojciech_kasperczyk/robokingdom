﻿namespace Map
{
    public abstract class GeneratorRule
    {
        public abstract void Generate(MapPrototype mapPrototype);
    }
}