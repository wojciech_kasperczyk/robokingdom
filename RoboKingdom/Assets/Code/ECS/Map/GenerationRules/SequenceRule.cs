﻿using System.Collections.Generic;

namespace Map
{
    public class SequenceRule : GeneratorRule
    {
        private readonly List<GeneratorRule> _generatorRules;

        public SequenceRule(List<GeneratorRule> rulesSequence)
        {
            _generatorRules = rulesSequence;
        }
        
        public override void Generate(MapPrototype mapPrototype)
        {
            for (int i = 0; i < _generatorRules.Count; i++)
            {
                _generatorRules[i].Generate(mapPrototype);
            }
        }
    }
}