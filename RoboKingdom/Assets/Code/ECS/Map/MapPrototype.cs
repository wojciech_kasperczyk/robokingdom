﻿using Components.Map;
using Settings;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Rendering;
using Unity.Transforms;

namespace Map
{
    public class MapPrototype
    {
        public readonly MapTilePrototype[,] TilesPrototype;
        public readonly int Width;
        public readonly int Heigth;
        
        private EntityArchetype _mapInfoArchetype;
        private EntityArchetype _mapTileArchetype;

        public MapPrototype(int width, int heigth)
        {
            Heigth = heigth;
            Width = width;
            TilesPrototype = new MapTilePrototype[width,heigth];
            for (int x = 0; x < Width; x++)
            {
                for (int y = 0; y < Heigth; y++)
                {
                    TilesPrototype[x,y].Position = new int2(x,y);
                }
            }
        }

        public void GenerateEntities(EntityManager manager, MapVisuals mapVisuals)
        {
            CreateArchetypes(manager);
            GenerateMapInfo(manager);
            GenerateMapTiles(manager,mapVisuals);
        }

        private void CreateArchetypes(EntityManager manager)
        {
            _mapInfoArchetype = manager.CreateArchetype(
                ComponentType.Create<MapInfo>(),
                ComponentType.Create<MapSize>());

            _mapTileArchetype = manager.CreateArchetype(
                ComponentType.Create<MapTile>(),
                ComponentType.Create<MapPosition>(),
                ComponentType.Create<TileType>(),
                ComponentType.Create<Position>(),
                ComponentType.Create<Rotation>(),
                ComponentType.Create<MeshInstanceRenderer>(),
                ComponentType.Create<TransformMatrix>());
            
        }

        private void GenerateMapInfo(EntityManager manager)
        {
            var mapInfo = manager.CreateEntity(_mapInfoArchetype);
            manager.SetComponentData(mapInfo,new MapSize(){Width = Width,Height = Heigth});
        }

        private void GenerateMapTiles(EntityManager manager, MapVisuals mapVisuals)
        {
            var mapVisualsData = mapVisuals.CreateMapVisualsData();
            
            var mapTiles = new NativeArray<Entity>(Width * Heigth,Allocator.Temp);
            manager.CreateEntity(_mapTileArchetype, mapTiles);
            
            for (int x = 0; x < Width; x++)
            {
                var columnOffset = x*Heigth;
                
                for (int y = 0; y < Heigth; y++)
                {
                    var entityIndex = columnOffset + y;
                    var entity = mapTiles[entityIndex];

                    var mapTilePrototype = TilesPrototype[x,y];
                    
                    float3 position = MapHelper.MapToWorldPosition(
                        mapTilePrototype.Position,
                        Width,
                        Heigth,
                        mapVisuals.TilesVisualOffset
                    );
                    
                    manager.SetComponentData(entity,new MapPosition() { Value = mapTilePrototype.Position});
                    manager.SetComponentData(entity,new TileType() {Value = mapTilePrototype.TileType});
                    manager.SetSharedComponentData(entity, mapVisualsData.GetTileInstanceRenderer(mapTilePrototype.TileType));
                    manager.SetComponentData(entity, new TransformMatrix());
                    manager.SetComponentData(entity, new Rotation());

                    manager.SetComponentData(entity, new Position(position));
                }
            }
            
            mapTiles.Dispose();
        }
    }
}