﻿using Unity.Mathematics;

namespace Map
{
    public struct MapTilePrototype
    {
        public int2 Position;
        public MapTileType TileType;
    }
}